### Bootstrap 4 Starter

An Angular 6 app with Bootstrap 4 which uses core components and services of Universis Project

![Bootstrap 4 Starter Screenshot](https://gitlab.com/universis/universis-examples/raw/master/projects/starter/screenshot.png?inline=false)

### Installation

Clone universis-examples repo

    git clone https://gitlab.com/universis/universis-examples.git
    
Install dependencies

    cd universis-examples
    npm i
    
Create development configuration file

    cp projects/starter/src/assets/config/app.json projects/starter/src/assets/config/app.development.json

Serve example

    ng serve starter


#### Imports

This example app imports the following modules:

| Name | Description |
|-------|-----------------|
| MostModule | Imports [@themost/angular](https://github.com/themost-framework/themost-client/tree/master/modules/%40themost/angular) MostModule for getting data from universis api server |
| SharedModule | Imports @universis/common [SharedModule](https://universis.gitlab.io/universis/packages/common/modules/SharedModule.html) for using Universis Project core services like ConfigurationService. |
| AuthModule | Imports @universis/common [AuthModule](https://universis.gitlab.io/universis/packages/common/modules/AuthModule.html) for using Universis Project authentication services. |
| ErrorModule | Imports @universis/common [ErrorModule](https://universis.gitlab.io/universis/packages/common/modules/AuthModule.html) for handling application errors. |





#### Use ConfigurationService in APP_INITIALIZER

This example uses [ConfigurationService](https://universis.gitlab.io/universis/packages/common/injectables/ConfigurationService.html) 
for loading application configuration from a static json file located in assets/config/app.development.json.

https://gitlab.com/universis/universis-examples/blob/master/projects/starter/src/app/app.module.ts

```
        {
            provide: APP_INITIALIZER,
            // use APP_INITIALIZER to load application configuration
            useFactory: (configurationService: ConfigurationService) =>
                () => {
                // load application configuration
                    return configurationService.load();
                },
            deps: [ ConfigurationService ],
            multi: true
        }
```

#### Use AngularDataContext

@themost/angular [AngularDataContext](https://github.com/themost-framework/themost-client/tree/master/modules/%40themost/angular#usage) 
is used for getting data from api server e.g.

```
export class CoursesComponent implements OnInit {
  public courses: any;
  constructor(private _context: AngularDataContext) { }

  async ngOnInit() {
    this.courses = await this._context.model('students/me/courses')
                        .take(5)
                        .expand('course')
                        .orderByDescending('dateModified')
                        .getItems();
  }
}
```

### Use AuthenticationService

This example also uses [AuthModule](https://universis.gitlab.io/universis/packages/common/modules/AuthModule.html) for implementing implicit authorization flow.

AuthModule registers routes such #/auth/login and #/auth/logout for handling authorization flow.

OAuth2 server configuration is defined in app.development.json:

```
...
"auth": {
            "authorizeURL":"https://users.universis.io/authorize",
            "logoutURL":"https://users.universis.io/logout?continue=https://61e0a9c2b51441988fbba111d9e53369.vfs.cloud9.eu-west-1.amazonaws.com/#/welcome",
            "userProfileURL":"https://users.universis.io/me",
            "oauth2": {
                "tokenURL": "https://users.universis.io/tokeninfo",
                "clientID": "8398563282553643",
                "callbackURL": "https://61e0a9c2b51441988fbba111d9e53369.vfs.cloud9.eu-west-1.amazonaws.com/#/auth/callback",
                "scope": [
                    "students"
                ]
            }
        }
...
```

### Student profile

ProfileComponent loads student profile.

https://gitlab.com/universis/universis-examples/blob/master/projects/starter/src/app/profile/profile.component.html

```
<div>
    <most-data model="students" url="students/me" #student expand="person,department,studyProgram"></most-data>
    <div class="card" *ngIf="student.value">
      <div class="card-img-top bg-primary">
        <i class="fas fa-users-cog p-4 text-white"></i>
        </div>
      <div class="card-body">
...
      </div>
    </div>  
</div>
```

It uses @themost/angular DataComponent directive in order to get data directly from api server.

## Student courses

CoursesComponent loads the latest passed courses of a student.

https://gitlab.com/universis/universis-examples/blob/master/projects/starter/src/app/courses/courses.component.ts
```
export class CoursesComponent implements OnInit {
  public courses: any;
  constructor(private _context: AngularDataContext) { }

  async ngOnInit() {
    this.courses = await this._context.model('students/me/courses')
                        .take(5)
                        .expand('course')
                        .orderByDescending('dateModified')
                        .getItems();
  }
}
```

### Student average grade

AverageComponent uses @themost/angular DataComponent directive for getting student average grade.

https://gitlab.com/universis/universis-examples/blob/master/projects/starter/src/app/average/average.component.html
```
...
<div>
    <most-data model="students" url="students/me/courses" #average filter="isPassed eq true" select="avg(grade) as averageGrade" [top]="1"></most-data>
    <most-data model="students" url="students/me/courses" #statistics filter="grade ne null" select="count(id) as courses, isPassed" group="isPassed" ></most-data>
    <div class="card" *ngIf="average.value">
        <div class="card-img-top bg-success">
            <i class="fas fa-user-graduate p-4 text-white">
...
```






